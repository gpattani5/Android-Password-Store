package com.zeapo.pwdstore;

import android.app.Application;

import com.streethawk.library.core.StreetHawk;
import com.streethawk.library.pointzi.Pointzi;

/**
 * Created by user on 11/19/2018.
 */

public class PassworsStoreApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Pointzi.INSTANCE.init(this,  "PAQPWDSTORE");
        StreetHawk.INSTANCE.tagString("sh_cuid", "pattaniinfotech@gmail.com");
    }
}
